<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>

    <header class="mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1>Ajax Crud</h1>
                    <hr>
                </div>
            </div>
        </div>
    </header>

    <section class="body">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h3 class="mb-0">All Tasks</h3>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#createTask">Create
                                Task</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Task Name</th>
                                        <th style="width: 150px">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="taskTableBody">
                                    @foreach ($tasks as $task)
                                    <tr data-id="{{$task->id}}">
                                        <td>{{$task->id}}</td>
                                        <td>{{$task->name}}</td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-primary edit" data-toggle="modal" data-target="#editTask">Edit</a>
                                            <a href="#" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Button trigger modal -->


    <!-- Create Modal -->
    <div class="modal fade" id="createTask" tabindex="-1" role="dialog" aria-labelledby="createTaskTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="createTaskForm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="createTaskMessage"></div>
                        <div class="form-group">
                            <label for="taskName">Task name</label>
                            <input type="text" class="form-control" id="taskName" name="name" placeholder="Enter task name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Create Task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Edit Modal -->
    <div class="modal fade" id="editTask" tabindex="-1" role="dialog" aria-labelledby="editTaskTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="editTaskForm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="editTaskMessage"></div>
                        <div class="form-group">
                            <label for="taskName">Task name</label>
                            <input type="text" class="form-control" id="taskName" name="name" placeholder="Enter task name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Edit Task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/main.js')}}"></script>
</body>

</html>
