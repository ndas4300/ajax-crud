
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#createTaskForm').submit(function (e) {
    e.preventDefault();

    let msg = $('#createTaskMessage');
    let input = $('#createTaskForm input[name="name"]');
    let formData = {
        name: $(input).val()
    }
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: '/task',
        data: formData,
        success: function (data) {

            $(msg).html('');
            $(msg).append('<div class="alert alert-success">Task Created Successfully</div>');
            $(input).val('');

            $('#taskTableBody').append(`
            <tr>
            <td>`+ data.id +`</td>
            <td>`+ data.name +`</td>
            <td>
                <a href="#" class="btn btn-sm btn-primary">Edit</a>
                <a href="#" class="btn btn-sm btn-danger">Delete</a>
            </td>
        </tr>
        `)
        },
        error: function (error) {
            $(msg).html('');
            $(msg).append(`<ul id="errorMassage" class="alert alert-danger"></ul>`)

            $.each(error.responseJSON.errors,function (index,value) {
                console.log(value[0]);
                $(msg).find('#errorMassage').append(`
                <li>`+ value[0]+`</li>
                `);
            });
        }
    })

});

$(document).on('click','.edit',function () {
    let task= $(this).closest('tr').data('id');
    let modal= $('#editTaskForm');

    $.ajax({
        type: 'GET',
        url: 'task/'+task+'/edit',
        success: function (data) {
            modal.find('#taskName').val(data.name);
            modal.attr('data-id',data.id);
        },
        error: function (error) {
            console.log(error);
        }

    });
});

$('#editTaskForm').submit(function (e) {
    e.preventDefault();

    let msg = $('#editTaskMessage');
    let input = $('#editTaskForm #taskName');
    let id = $(this).data('id');

    let formData = {
        name: $(input).val()
    }
    console.log(formData);
    $.ajax({
        type: 'PUT',
        url: 'task/'+id,
        data: formData,
        success: function (data) {

            $(msg).html('');
            $(msg).append('<div class="alert alert-success">Task Created Successfully</div>');
            $(input).val('');

            $('#taskTableBody').append(`
            <tr>
            <td>`+ data.id +`</td>
            <td>`+ data.name +`</td>
            <td>
                <a href="#" class="btn btn-sm btn-primary">Edit</a>
                <a href="#" class="btn btn-sm btn-danger">Delete</a>
            </td>
        </tr>
        `)
        },
        error: function (error) {
            $(msg).html('');
            $(msg).append(`<ul id="errorMassage" class="alert alert-danger"></ul>`)

            $.each(error.responseJSON.errors,function (index,value) {
                console.log(value[0]);
                $(msg).find('#errorMassage').append(`
                <li>`+ value[0]+`</li>
                `);
            });
        }
    })

});
